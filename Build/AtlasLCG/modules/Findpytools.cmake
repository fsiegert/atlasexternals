# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#  PYTOOLS_BINARY_PATH
#  PYTOOLS_PYTHON_PATH
#
# Can be steered by PYTOOLS_ROOT.
#

# The LCG include(s):
include( LCGFunctions )

# If it was already found, let's be quiet:
if( PYTOOLS_FOUND )
   set( pytools_FIND_QUIETLY TRUE )
endif()

# Ignore system paths when an LCG release was set up:
if( PYTOOLS_ROOT )
   set( _extraPyToolsArgs NO_SYSTEM_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH )
endif()

# Find the binary path:
find_path( PYTOOLS_BINARY_PATH ipython
   PATH_SUFFIXES bin PATHS ${PYTOOLS_ROOT}
   ${_extraPyToolsArgs} )

# Find the python path:
find_path( PYTOOLS_PYTHON_PATH 
   NAMES site.py decorator.py
   PATH_SUFFIXES lib/python2.7/site-packages
   PATHS ${PYTOOLS_ROOT}
   ${_extraPyToolsArgs} )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( pytools DEFAULT_MSG
   PYTOOLS_PYTHON_PATH PYTOOLS_BINARY_PATH )

# Set up the RPM dependency:
lcg_need_rpm( pytools )

# Clean up:
if( _extraPyToolsArgs )
   unset( _extraPyToolsArgs )
endif()
