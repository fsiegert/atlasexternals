# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# CMake configuration file building HepMCAnalysis ad part of the
# offline build.
#

# The name of the package:
atlas_subdir( HepMCAnalysis )

# In release recompilation mode finish here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# LCG packages that the build depends on:
find_package( HepMC )
find_package( ROOT )

# Temporary location for the build results:
set( _buildDir
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/HepMCAnalysisBuild )

# Build HepMCAnalysis for the build area:
ExternalProject_Add( HepMCAnalysis
  PREFIX ${CMAKE_BINARY_DIR}
  URL http://cern.ch/service-spi/external/MCGenerators/distribution/hepmcanalysis/hepmcanalysis-3.4.14-src.tgz
  URL_MD5 466bd591f3ab112e52a1b223b74dd6b1
  BUILD_IN_SOURCE 1
  PATCH_COMMAND ${CMAKE_COMMAND} -E copy_directory
  <SOURCE_DIR>/3.4.14/ <SOURCE_DIR>
  COMMAND ${CMAKE_COMMAND} -E remove_directory <SOURCE_DIR>/3.4.14
  COMMAND patch -p1 <
  ${CMAKE_CURRENT_SOURCE_DIR}/patches/hepmcanalysis-3.4.14-src.patch
  CONFIGURE_COMMAND ${CMAKE_COMMAND} -E echo
  "No configuration step for HepMCAnalysis"
  BUILD_COMMAND ${CMAKE_BINARY_DIR}/atlas_build_run.sh make
  HepMCdir=${HEPMC_ROOT} FastJetdir=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
  ROOTSYS=${ROOT_ROOT} CLHEPdir=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory lib/ ${_buildDir}/lib/
  COMMAND ${CMAKE_COMMAND} -E copy_directory lib/
  ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/
  COMMAND ${CMAKE_COMMAND} -E copy_directory include/ ${_buildDir}/include/
  COMMAND ${CMAKE_COMMAND} -E copy_directory include/
  ${CMAKE_INCLUDE_OUTPUT_DIRECTORY}/ )
add_dependencies( HepMCAnalysis CLHEP FastJet )
add_dependencies( Package_HepMCAnalysis HepMCAnalysis )

# Install HepMCAnalysis:
install( DIRECTORY ${_buildDir}/
  DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
